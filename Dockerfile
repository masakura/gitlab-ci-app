FROM mcr.microsoft.com/dotnet/aspnet:3.1

WORKDIR /webapp

ADD publish /webapp

EXPOSE 80
ENTRYPOINT ["dotnet", "GitLabCIApp.dll"]
